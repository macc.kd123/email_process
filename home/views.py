import re
from django.shortcuts import render, redirect
from home.email_process import EmailClass
from django.contrib import messages

# Create your views here.

def home(request):
    regex = r'^[a-zA-Z0-9-_]+@[a-zA-Z0-9]+\.[a-z]{1,3}$'

    if request.method == 'POST':
        send_to_email = request.POST['email']
        subject_of_email = request.POST['subject']
        content = request.POST['content']
        print(send_to_email, subject_of_email, content)

        if (re.fullmatch(regex, send_to_email)):
            messages.error(request, 'please fill Email correctly.')
        else:
            send_email = EmailClass()
            #call mailgun api function
            send_status = send_email.mailgun(send_to_email, subject_of_email, content)

            if send_status == 'yes':
                messages.success(request, 'Your message has been successfuly send to {0}'.format(send_to_email))
            elif send_status == 'no':
                # call mailchamp api function
                send_status_mailchamp = send_email.mailchampa(send_to_email, subject_of_email, content)
                if send_status_mailchamp == 'yes':
                    messages.success(request, 'Your message has been successfuly send to {0}'.format(send_to_email))
                else:
                    messages.success(request, 'Email not send at {0}'.format(send_to_email))
            else:
                messages.success(request, 'Email not send at {0}'.format(send_to_email))

    return render(request, 'home/email_send.html', locals())
