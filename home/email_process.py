from __future__ import print_function
import sib_api_v3_sdk
from sib_api_v3_sdk.rest import ApiException
import mailchimp_transactional as MailchimpTransactional
from mailchimp_transactional.api_client import ApiClientError

import sendgrid
import os
from sendgrid.helpers.mail import Mail, Email, To, Content

class EmailClass:

    def __init__(self):
        self.sender = 'makarandkadam03@gmail.com'

    # call mailgun api
    def mailgun(self, send_to_email, subject_of_email, content):
        send_status = None
        configuration = sib_api_v3_sdk.Configuration()
        configuration.api_key[
            'api-key'] = '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'

        api_instance = sib_api_v3_sdk.TransactionalEmailsApi(sib_api_v3_sdk.ApiClient(configuration))
        subject = subject_of_email
        sender = {"name": "Makarand", "email": self.sender}
        replyTo = {"name": "Makarand", "email": send_to_email}
        html_content = "<html><body>Hi, <br><p>{0}</p></body></html>".format(content)
        to = [{"email": send_to_email, "name": "Makarand"}]
        params = {"parameter": "My param value", "subject": subject}
        # send_smtp_email = sib_api_v3_sdk.SendSmtpEmail(to=to, bcc=bcc, cc=cc, reply_to=reply_to, headers=headers, html_content=html_content, sender=sender, subject=subject)
        send_smtp_email = sib_api_v3_sdk.SendSmtpEmail(to=to, html_content=html_content, sender=sender, subject=subject)

        try:
            api_response = api_instance.send_transac_email(send_smtp_email)
            print(api_response)
            send_status = 'yes'
        except ApiException as e:
            print("Exception when calling SMTPApi->send_transac_email: %s\n" % e)
            send_status = 'no'
        return send_status

    # call mailchamp api
    def mailchampa(self, send_to_email, subject_of_email, content):
        send_status = None
        mailchimp = MailchimpTransactional.Client('FsMxTMAyRLs5tq9neNqUDw')
        message = {
            "from_email": "{0}".format(self.sender),
            "subject": "{0}".format(subject_of_email),
            "text": "<html><body>Hi, <br><p>{0}</p></body></html>".format(content),
            "to": [{"email": "{0}".format(send_to_email),
                    "type": "to"}] }

        try:
            response = mailchimp.messages.send({"message": message})
            print('API called successfully: {}'.format(response))
            if response[0]['status'] == 'success':
                send_status = 'yes'
            else:
                send_status = 'no'
        except ApiClientError as error:
            print('An exception occurred: {}'.format(error.text))
            # send_status = 'no'
            # send grid api call if exception occur
            send_status = self.sendgridapi(send_to_email, subject_of_email, content)

        return send_status

    # call sendgrid api
    def sendgridapi(self, send_to_email, subject_of_email, content):
        send_status = None
        api = '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'
        sg = sendgrid.SendGridAPIClient(api_key=os.environ.get(api))
        from_email = Email(self.sender)  # Change to your verified sender
        to_email = To(send_to_email)  # Change to your recipient
        subject = subject_of_email
        content = Content("text/plain", "<html><body>Hi, <br><p>{0}</p></body></html>".format(content))
        mail = Mail(from_email, to_email, subject, content)

        # Get a JSON-ready representation of the Mail object
        mail_json = mail.get()

        # Send an HTTP POST request to /mail/send


        try:
            response = sg.client.mail.send.post(request_body=mail_json)
            print(response.status_code)
            print(response.headers)
            if response.status_code == '200':
                send_status = 'yes'
            else:
                send_status = 'no'
        except ApiClientError as error:
            print('An exception occurred: {}'.format(error.text))
            send_status = 'no'

        return send_status
